var express = require('express');
var mysql = require('mysql');

var connection = mysql.createConnection({
		host: 'localhost',
		user: 'root',
		password: 'administrator',
		database: 'Stellwag'
});

connection.connect(function(err) {
	if(!err) 
		console.log('Baza de date conectata');
	else console.log ('Error connecting database');
});

var app = express();
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();

app.use("/css", express.static(__dirname + '/css'));
app.use("/js", express.static(__dirname + '/js'));
app.use("/data.txt", express.static(__dirname + '/data.txt'));
app.use("/logo.png", express.static(__dirname + '/logo.png'));
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', function(req,res) {
	res.sendFile(__dirname + '/index2.html');
});

app.post('/id', function(req,res) {
	
	var data = req.body;
	var id = data.id;
	//console.log(id);
	var query = "SELECT * FROM Control WHERE id=" +id;
	connection.query(query, function(error, result) {
			//console.log(result);
			res.json(result);
	});
});

app.post('/alert', function(req,res) {
	
	var query = "SELECT max(id) as id FROM Control";
	connection.query(query, function(error, result) {
			console.log(result);
			res.json(result);
	
	});
});

app.post('/update', function(req,res) {
	
	var data = req.body;
	var id = data.id;
	
	var query = "UPDATE Control SET casate='"+ req.body.casate +"', reparate='"+ req.body.reparate +"' WHERE id=" + id;
	console.log(query);
	connection.query(query, function(error, result) {
			console.log("Modificat cu succes! " );
	});
});


app.post('/', jsonParser, function(req,res) {
	//var query = "INSERT INTO Test(Data, kw , Nr Articol , Optic) VALUES('" + req.body.data + "' , '" + req.body.kw + "' , '" + req.body.art + "' , '"+ req.body.optic + "');";	
	/*connection.query(query, function() {
			console.log('Trimis cu succes');
	}); */
	
	var query = "INSERT INTO Control(";
	
	
	var json = JSON.stringify(req.body);
	var object = JSON.parse(json);
	var nameStarted = false;
	var valueStarted = false;

	var collumns = [];
	var values = [];
	for(var i = 0; i < object.length; i++) {
		if(object[i].value) {
			collumns.push(object[i].name);
			values.push("'" + object[i].value + "'");
			
			switch(object[i].name) {
				
				case 'optic':
					var total = object[object.length - 1].value;
					var ppm = total / object[i].value * 1000000.0;
					insertDb(object[1].value,'optic', object[i].value, ppm, total);
					break;
				
				case 'electric':
				
					var total = object[object.length - 1].value;
					var ppm = total / object[i].value * 1000000.0;
					insertDb(object[1].value,'electric', object[i].value, ppm, total);
					break;
				
				case 'reelectric':
					var total = object[object.length - 1].value;
					var ppm = total / object[i].value * 1000000.0;
					insertDb(object[1].value,'reelectric', object[i].value, ppm, total);
					break;
				
			}
		}	
	}
	
	query += collumns.join(',');
	query += ') VALUES(';
	query+= values.join(',');
	query+= ');';
	
	
	
	console.log(query);
	
	connection.query(query, function() {
			console.log('Trimis cu succes');
	});

	
	
});


function insertDb(article, collumn, value, ppm, total) {
	
	var query = "INSERT INTO PPM (nr_articol," + collumn + ", total ,ppm) VALUES('"+ article + "', '"+ value+ "', '" + total + "', '"+ ppm + "' )"; 
	connection.query(query, function() {
		console.log('PPM inserat cu succes');
	
	});
};

app.listen(3000);


